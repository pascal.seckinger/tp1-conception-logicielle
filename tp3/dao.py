import sqlite3 as sgbd


class AppDB:
    def __init__(self):
        self.__connection = sgbd.connect("app.db")

    def init(self):
        query = (
            "DROP TABLE IF EXISTS requete;"
            "CREATE TABLE requete ("
            "id integer PRIMARY KEY autoincrement,"
            "label text NOT NULL,"
            "date date"
            ");"
        )
        with self.__connection as connection:
            cursor = connection.cursor()
            cursor.executescript(query)
            connection.commit()

    def add_requete(self, label, date):
        query = f"INSERT INTO requete (label, date) VALUES ('{label}', '{date}');"
        with self.__connection as connection:
            cursor = connection.cursor()
            cursor.execute(query)

    def get_requetes(self):
        query = "SELECT * FROM requete;"
        with self.__connection as connection:
            cursor = connection.cursor()
            cursor.execute(query)
            requetes = cursor.fetchall()
        return requetes


if __name__ == "__main__":
    db = AppDB()
    db.init()
    db.add_requete("numero 1", "11-28-1980")
    requetes = db.get_requetes()
    print(requetes)
