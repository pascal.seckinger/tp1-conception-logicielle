from sqlalchemy import create_engine

engine = create_engine("sqlite:///:memory:")

with engine.connect() as connection:

    connection.execute("DROP TABLE IF EXISTS requete")
    connection.execute(
        """CREATE TABLE requete(requete_id INTEGER PRIMARY KEY AUTOINCREMENT, date_text text)"""
    )

    data = (
        {"requete_id": 1, "date_text": "2022-03-15 11:35:58"},
        {"requete_id": 2, "date_text": "2022-03-15 12:46:38"},
    )

    for line in data:
        connection.execute(
            """INSERT INTO requete(requete_id, date_text)
        VALUES(:requete_id, :date_text)""",
            **line
        )

    result = connection.execute("""SELECT * from requete""")

    for row in result:
        print(row)
