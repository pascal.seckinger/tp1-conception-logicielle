from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Sequence
from sqlalchemy import create_engine, select
from sqlalchemy.orm import Session

Base = declarative_base()


class Requete(Base):
    __tablename__ = "requete"
    id = Column(Integer, Sequence("requete_id_seq"), primary_key=True)
    label = Column(String(50))
    date = Column(String(50))


engine = create_engine("sqlite:///app.db", echo=True, future=True)
Base.metadata.create_all(engine)

with Session(engine) as session:
    requete1 = Requete(label="requete1", date="1980-11-28")
    requete2 = Requete(label="requete2", date="2020-01-01")

    session.add_all([requete1, requete2])

    session.commit()

    requetes = session.query(Requete).all()

    print(requetes)
